<?php

namespace Sws\Exception;

use Psr\Container\ContainerExceptionInterface;
use Throwable;

class FuncNotFoundException extends \InvalidArgumentException implements ContainerExceptionInterface
{
    private $func;

    public function __construct($message, $func, Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
        $this->func = $func;
    }

    public function getFunc()
    {
        return $this->func;
    }
}
