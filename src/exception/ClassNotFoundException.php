<?php

namespace Sws\Exception;

use Psr\Container\ContainerExceptionInterface;
use Throwable;

class ClassNotFoundException extends \InvalidArgumentException implements ContainerExceptionInterface
{
    private $class;

    public function __construct($message, $class, Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
        $this->class = $class;
    }

    public function getClass()
    {
        return $this->class;
    }
}
