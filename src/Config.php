<?php

namespace Sws;

class Config
{

    /**
     * 配置项
     * @var array
     */
    private array $config = [];


    /**
     * 加载目录到配置项
     * @param string $path 路径
     * @param array $filter 要排除的文件或目录
     * @return void
     */
    public function load(string $path, array $filter = [])
    {
        $this->parse($path, $filter, $this->config);
    }


    /**
     * 解析路径
     * @param string $path
     * @param array $filter
     * @param array $config
     * @return void
     */
    private function parse(string $path, array $filter, array &$config)
    {
        $files = scandir($path);

        foreach ($files as $file) {

            if ($file == '.' || $file == '..' || in_array($file, $filter)) {
                continue;
            }

            if (is_dir($path . DIRECTORY_SEPARATOR . $file)) {

                $c = [];
                $this->parse($path . DIRECTORY_SEPARATOR . $file, $filter, $c);

                if (!empty($c)) {
                    $config[$file] = $c;
                }
                continue;
            }

            if (is_file($path . DIRECTORY_SEPARATOR . $file)) {

                $pathInfo = pathinfo($file);

                switch ($pathInfo['extension']) {
                    case 'php':
                        $configValue = include $path . DIRECTORY_SEPARATOR . $file;
                        break;
                    case 'yml':
                    case 'yaml':
                        if (function_exists('yaml_parse_file')) {
                            $configValue = yaml_parse_file($path . DIRECTORY_SEPARATOR . $file);
                        }
                        break;
                    case 'ini':
                        $configValue = parse_ini_file($path . DIRECTORY_SEPARATOR . $file, true, INI_SCANNER_TYPED) ?: [];
                        break;
                    case 'json':
                        $configValue = json_decode(file_get_contents($path . DIRECTORY_SEPARATOR . $file), true);
                        break;
                }

                if (isset($configValue)) {
                    $config[$pathInfo['filename']] = $configValue;
                }

            }
        }
    }


    /**
     * 获取配置项
     * @param string $key
     * @param $default
     * @return array|mixed|null
     */
    public function get(string $key = '', $default = null)
    {
        if (!$key) {
            return $this->config;
        }

        $keyArray = explode('.', $key);
        $count = count($keyArray);

        $config = $this->config;
        $i = 0;
        while (true) {
            if ($i == $count) {
                break;
            }

            if (!isset($config[$keyArray[$i]])) {
                return $default;
            }
            $config = $config[$keyArray[$i]];
            $i++;
        }
        return $config;
    }

    /**
     * 设置配置项
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($key, $value = null): bool
    {
        if (is_array($key)) {
            $this->config = array_merge($this->config, $key);
            return true;
        }
        if (is_string($key)) {
            $keyArray = explode('.', $key);
            $this->setValue($keyArray, $value, $this->config);
            return true;
        }
        return false;
    }

    /**
     * @param array $keyArray
     * @param $value
     * @param array $config
     * @return void
     */
    private function setValue(array $keyArray, $value, array &$config)
    {
        if (count($keyArray) == 1) {
            $config[$keyArray[0]] = $value;
            return;
        }

        $key = array_shift($keyArray);
        if (!isset($config[$key])) {
            $config[$key] = [];
        }
        $this->setValue($keyArray, $value, $config[$key]);
    }

    /**
     * 配置项是否存在
     * @param $key
     * @return bool
     */
    public function has($key): bool
    {
        $keyArray = explode('.', $key);
        $count = count($keyArray);

        $config = $this->config;
        $i = 0;
        while (true) {
            if ($i == $count) {
                break;
            }

            if (!isset($config[$keyArray[$i]])) {
                return false;
            }
            $config = $config[$keyArray[$i]];
            $i++;
        }
        return true;
    }


}