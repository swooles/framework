<?php

namespace Sws\Di;

interface ContainerInterface extends \Psr\Container\ContainerInterface
{
    public function make(string $name, array $args = [], bool $newInstance = false);

    public function bind($name, $abstract = null);

    public function invoke($callable, array $args = [], bool $newInstance = false);

    public function delete(string $name);
}
