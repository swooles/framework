<?php

declare(strict_types=1);

namespace Sws\Di;

use Closure;
use ReflectionClass;
use ReflectionException;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use ReflectionMethod;
use Swoole\Coroutine;
use Sws\Exception\ClassNotFoundException;
use Sws\Exception\FuncNotFoundException;
use Sws\Exception\InvalidArgumentException;
use Sws\Exception\NotFoundException;

class Container implements ContainerInterface
{
    /**
     * singleton instance
     * key 0 is shared,other key is protected。
     * @var array
     */
    protected array $instances = [];

    /**
     * alias binding
     * @var array
     */
    protected array $bind = [];

    /**
     * Create Instance
     * @param string $name
     * @param array $args
     * @param bool $newInstance Create new instance
     * @return mixed
     */
    public function make(string $name, array $args = [], bool $newInstance = false)
    {
        $name = $this->getName($name);

        if ($this->has($name) && false === $newInstance) {
            return $this->get($name);
        }

        if (isset($this->getBind()[$name]) && $this->getBind()[$name] instanceof Closure) {
            $object = $this->invokeFunction($this->bind[$name], $args);
        } else {
            $object = $this->invokeClass($name, $args);
        }

        if (!$newInstance) {
            $this->setInstance($name, $object);
        }

        return $object;
    }

    /**
     * Get name by alias
     * @param string $alias
     * @return string
     */
    protected function getName(string $alias): string
    {
        $bind = $this->getBind();

        if (isset($bind[$alias])) {
            $name = $bind[$alias];
            if (is_string($name)) {
                return $this->getName($name);
            }
        }
        return $alias;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function has(string $id): bool
    {
        $name = $this->getName($id);
        return isset($this->getInstance()[$name]);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function get(string $id)
    {
        if ($this->has($id)) {
            $name = $this->getName($id);
            return $this->getInstance()[$name];
        }

        throw new NotFoundException("Container not Found '$id'");
    }

    /**
     * Invoke function
     * @param $function
     * @param array $args
     * @return mixed
     */
    protected function invokeFunction($function, array $args = [])
    {
        try {
            $reflect = new ReflectionFunction($function);
        } catch (ReflectionException $e) {
            throw new FuncNotFoundException("function not exists: $function()", $function);
        }
        $args = $this->bindParams($reflect, $args);
        return call_user_func_array($function, $args);
    }

    /**
     * Get reflection function params
     * @param ReflectionFunctionAbstract $reflect
     * @param array $var
     * @return array
     */
    protected function bindParams(ReflectionFunctionAbstract $reflect, array $var = []): array
    {
        $args = [];
        $isAssoc = (array_keys($var) === range(0, count($var) - 1));
        foreach ($reflect->getParameters() as $parameter) {
            $name = $parameter->getName();
            if ($class = $parameter->getClass()) {
                $args[] = $this->getObjectParam($class->getName(), $var);
            } elseif (!$isAssoc && isset($var[$name])) {
                $args[] = $var[$name];
            } elseif ($isAssoc && !empty($var)) {
                $args[] = array_shift($var);
            } elseif ($parameter->isDefaultValueAvailable()) {
                $args[] = $parameter->getDefaultValue();
            } else {
                throw new InvalidArgumentException('method param miss:' . $name);
            }
        }
        return $args;
    }

    /**
     * Get object param
     * @param $className
     * @param array $var
     * @return mixed
     */
    protected function getObjectParam($className, array &$var)
    {
        $array = $var;

        foreach ($array as $k => $item) {
            if ($item instanceof $className) {
                unset($var[$k]);
                return $item;
            }
        }
        return $this->make($className);
    }

    /**
     * Invoke class
     * @param string $class
     * @param array $args
     * @return object
     */
    protected function invokeClass(string $class, array $args = []): object
    {
        try {
            $reflect = new ReflectionClass($class);
        } catch (ReflectionException $e) {
            throw new ClassNotFoundException('class not exists: ' . $class, $class, $e);
        }

        $constructor = $reflect->getConstructor();
        $args = $constructor ? $this->bindParams($constructor, $args) : [];
        return $reflect->newInstanceArgs($args);
    }

    /**
     * Bind aliases, closures, instances, to containers
     * @param $name
     * @param $abstract
     * @return void
     */
    public function bind($name, $abstract = null)
    {
        if (is_array($name)) {
            foreach ($name as $key => $value) {
                $this->bind($key, $value);
            }
        } elseif (is_string($name)) {
            $name = $this->getName($name);

            if ($abstract instanceof Closure) {
                $this->setBing($name, $abstract);
            } elseif (is_object($abstract)) {
                $class = get_class($abstract);

                if (false !== $class && $class != $name) {
                    $this->setBing($name, $class);
                    $name = $class;
                }
                $this->setInstance($name, $abstract);
            } else {
                if ($name != $abstract && !is_null($abstract)) {
                    $this->setBing($name, $abstract);
                }
            }
        }
    }

    /**
     * @param $callable
     * @param array $args
     * @param bool $newInstance
     * @return mixed
     */
    public function invoke($callable, array $args = [], bool $newInstance = false)
    {
        if ($callable instanceof Closure) {
            return $this->invokeFunction($callable, $args);
        } elseif (is_string($callable) && false === strpos($callable, '::')) {
            return $this->invokeFunction($callable, $args);
        } else {
            return $this->invokeMethod($callable, $args, $newInstance);
        }
    }

    /**
     * @param $method
     * @param array $args
     * @param bool $newInstance
     * @return mixed
     */
    protected function invokeMethod($method, array $args = [], bool $newInstance = false)
    {
        if (is_array($method)) {
            [$class, $method] = $method;

            if (is_string($class) && $this->has($class) && !$newInstance) {
                $class = $this->get($class);
            } else {
                $class = is_object($class) ? get_class($class) : $this->invokeClass($class);
            }

        } else {
            [$class, $method] = explode('::', $method);
        }

        try {
            $reflect = new ReflectionMethod($class, $method);
        } catch (ReflectionException $e) {
            $class = is_object($class) ? get_class($class) : $class;
            throw new FuncNotFoundException('method not exists: ' . $class . '::' . $method . '()', "$class::$method", $e);
        }

        $args = $this->bindParams($reflect, $args);
        return $reflect->invokeArgs(is_object($class) ? $class : null, $args);
    }

    /**
     * delete instance
     * @param string $name
     * @return void
     */
    public function delete(string $name)
    {

        $name = $this->getName($name);
        if (!extension_loaded('swoole')) {
            unset($this->instances[$name]);
        }

        $cid = Coroutine::getCid();
        if ($cid === -1) {
            unset($this->instances[$name]);
        }

        $context = Coroutine::getContext();
        unset($context['#container']['instances'][$name]);
    }

    /**
     * Get instances
     * @return array
     */
    public function getInstances(): array
    {
        return $this->getInstance();
    }

    /**
     * Get binds
     * @return array
     */
    public function getBinds(): array
    {
        return $this->getBind();
    }

    protected function getInstance(): array
    {
        if (!extension_loaded('swoole')) {
            return $this->instances;
        }

        $cid = Coroutine::getCid();
        if ($cid === -1) {
            return $this->instances;
        }

        $context = Coroutine::getContext();
        if (!isset($context['#container']['instances'])) {
            $context['#container']['instances'] = [];
        }

        return array_merge($this->instances, $context['#container']['instances']);
    }

    protected function setInstance($name, $abstract = null)
    {
        if (!extension_loaded('swoole')) {
            $this->instances[$name] = $abstract;
            return;
        }

        $cid = Coroutine::getCid();
        if ($cid === -1) {
            $this->instances[$name] = $abstract;
            return;
        }
        $context = Coroutine::getContext();
        $context['#container']['instances'][$name] = $abstract;
    }


    protected function getBind(): array
    {
        if (!extension_loaded('swoole')) {
            return $this->bind;
        }

        $cid = Coroutine::getCid();
        if ($cid === -1) {
            return $this->bind;
        }

        $context = Coroutine::getContext();
        if (!isset($context['#container']['bing'])) {
            $context['#container']['bing'] = [];
        }

        return array_merge($this->bind, $context['#container']['bing']);
    }


    protected function setBing($name, $abstract = null)
    {
        if (!extension_loaded('swoole')) {
            $this->bind[$name] = $abstract;
            return;
        }

        $cid = Coroutine::getCid();
        if ($cid === -1) {
            $this->bind[$name] = $abstract;
            return;
        }
        $context = Coroutine::getContext();
        $context['#container']['bing'][$name] = $abstract;
    }


}
