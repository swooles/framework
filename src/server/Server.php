<?php

namespace Sws\Server;

use Swoole\Process;
use Swoole\Server as SwooleServer;
use Sws\App;
use Sws\Console\Console;

class Server
{


    protected App $app;


    /**
     * swoole服务
     * @var SwooleServer
     */
    protected SwooleServer $swooleServer;

    protected string $name;
    protected array $config;
    protected string $host;
    protected string $port;
    protected string $mode;
    protected string $sockType;
    protected array $setting;
    protected string $pidFile;
    protected array $event;


    public function __construct(App $app, array $config)
    {
        $this->app = $app;

        $this->config = $config;
        $this->host = $config['host'];
        $this->port = (int)$config['port'];
        $this->mode = $config['mode'];
        $this->sockType = $config['sock_type'] ?? '';
        $this->setting = $config['setting'];
        $this->pidFile = $config['setting']['pid_file'];
        $this->event = $config['event'];
    }

    /**
     * 事件绑定
     * @return void
     */
    protected function bindEvent()
    {
        foreach ($this->event as $key => $event) {
            $this->app->event->listen($key, $event);
            $this->swooleServer->on(substr($key, 2), function (...$args) use ($key) {
                $this->app->event->trigger($key, func_get_args());
            });
        }
    }


    /**
     * 获取master进程id
     * @return int
     */
    public function getPid(): int
    {
        if (is_readable($this->pidFile)) {
            return (int)file_get_contents($this->pidFile);
        }
        return 0;
    }

    /**
     * 判断master进程是否运行
     * @return bool
     */
    public function isRunning(): bool
    {
        $pid = $this->getPid();
        return $pid && Process::kill($pid, 0);
    }

    /**
     * 杀死进程
     * @param     $signal
     * @param int $wait
     * @return bool
     */
    protected function killProcess($signal, int $wait = 0): bool
    {
        $pid = $this->getPid();
        $pid > 0 && Process::kill($pid, $signal);

        if ($wait > 0) {
            $start = time();
            do {
                usleep(100000);
                if (!$this->isRunning()) {
                    break;
                }
            } while (time() < $start + $wait);
        }
        return $this->isRunning();
    }

    /**
     * 停止服务
     * @return void
     */
    public function stop()
    {
        if (!$this->isRunning()) {
            $this->app->console->writeln($this->name . 'server is not running.', Console::ERROR);
            return;
        }

        $this->app->console->writeln("stopping {$this->name} server...");
        if ($this->killProcess(SIGTERM, 10)) {
            $this->app->console->writeln("Unable to stop server.", Console::ERROR);
            return;
        }

        is_file($this->pidFile) && @unlink($this->pidFile);
        $this->app->console->writeln("stopping success.", Console::SUCCESS);
    }

    /**
     * 重启服务
     * @return void
     */
    public function restart()
    {
        if ($this->isRunning()) {
            $this->stop();
        }
        $this->start();
    }

    /**
     * 热重启服务
     * @return void
     */
    public function reload()
    {
        if (!$this->isRunning()) {
            $this->app->console->writeln($this->name . " server is not running.", Console::ERROR);
            return;
        }

        $this->app->console->writeln("reloading {$this->name} server...");
        if ($this->killProcess(SIGUSR1, 3)) {
            $this->app->console->writeln("Unable to stop server.", Console::ERROR);
            return;
        }
        $this->app->console->writeln("reloading success.", Console::SUCCESS);
    }


}