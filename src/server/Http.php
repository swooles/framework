<?php

namespace Sws\Server;

use Closure;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Sws\App;
use Sws\Console\Console;
use Sws\Exception\RouteNotFoundException;
use Throwable;

class Http extends Server
{


    public function __construct(App $app, array $config)
    {
        parent::__construct($app, $config);
        $this->name = 'http';
    }

    /**
     * 启动服务
     * @return void
     */
    public function start()
    {
        if ($this->isRunning()) {
            $this->app->console->writeln('The service is running.', Console::ERROR);
            return;
        }

        $this->swooleServer = new \Swoole\Http\Server($this->host, $this->port, $this->mode, $this->sockType);
        $this->swooleServer->set($this->setting);
        $this->bindEvent();
        $this->app->event->trigger('serverCreate');

        $this->app->console->writeln($this->app->console->logo());
        $this->app->console->writeln('Server started success: <http' . (isset($this->setting['ssl_cert_file']) ? 's' : '') . '://' . $this->host . ':' . $this->port . '>', Console::SUCCESS);

        $this->swooleServer->start();
    }

    /**
     *
     * @param Request $request
     * @param Response $response
     * @return void
     */
    public function onRequest(Request $request, Response $response)
    {
        $this->app->container->bind(['request' => $request, 'response' => $response]);

        try {
            $match = $this->app->route->match($request->server['request_method'], $request->server['request_uri']);

            if (null === $match) {
                throw new RouteNotFoundException(404, 'Route Not Found.');
            }
            list($isDynamic, $list) = $match;

            $middlewares = array_reverse($list['middleware']);
            array_unshift($middlewares, $list['handle']);

            $next = null;
            foreach ($middlewares as $middleware) {
                //把请求对象当做参数传给闭包
                $next = function ($request) use ($next, $middleware, $list, $isDynamic) {
                    //如果执行到了目标路由
                    if ($middleware == $list['handle']) {

                        //如果是动态路由，构建方法参数
                        $variable = [];
                        if (true === $isDynamic) {
                            foreach ($list['variable'] as $k => $value) {
                                $variable[$value] = $list['matches'][$k + 1];
                            }
                        }
                        return $this->app->container->invoke($middleware, $variable);
                    }

                    if ($middleware instanceof Closure) {
                        return $this->app->container->invoke($middleware, [$request, $next]);
                    } else {
                        return $this->app->container->invoke([$middleware, 'handle'], [$request, $next]);
                    }
                };
            }
            $responseData = $next($request);

        } catch (Throwable $e) {
            $exceptionHandle = $this->config['exceptionHandle'] ?? null;
            if (empty($exceptionHandle)) {
                $exceptionHandle = [Http::class, 'exceptionHandle'];
            }
            $responseData = $this->app->container->invoke($exceptionHandle, [$e]);
        } finally {
            if (true === $response->isWritable()) {
                if (is_array($responseData) || is_object($responseData)) {
                    $response->setHeader('Content-Type', 'application/json');
                    $responseData = json_encode($responseData);
                }
                $response->end((string)$responseData);
            }
        }
    }


    /**
     * 异常处理
     * @param Throwable $e
     * @return string|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function exceptionHandle(Throwable $e): ?string
    {
        /**@var Response $response */
        $response = $this->app->container->get('response');

        if (get_class($e) == RouteNotFoundException::class) {
            $response->status($e->getStatusCode());
            return $e->getMessage();
        }
        $response->status(500);

        if ($this->app->getDebug()) {
            return "{$e->getMessage()} ({$e->getFile()})[{$e->getLine()}]";
        } else {
            return $e->getMessage();
        }
    }

}