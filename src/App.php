<?php

namespace Sws;

use Exception;
use Sws\Console\Console;
use Sws\Di\ContainerInterface;
use Sws\Route\Route;

/**
 * @property Config $config
 * @property Console $console
 * @property Event $event
 * @property Route $route
 */
class App
{
    const VERSION = '1.0.0';

    /**
     * 调试模式
     * @var bool
     */
    private bool $debug = false;

    /**
     * Di容器
     * @var ContainerInterface
     */
    public ContainerInterface $container;

    /**
     * 应用实例
     * @var App
     */
    protected static App $instance;

    /**
     * 是否初始化
     * @var bool
     */
    protected bool $isInitialize = false;

    /**
     * 应用根目录
     * @var string
     */
    protected string $rootPath;


    /**
     * 容器绑定标识
     * @var array|string[]
     */
    protected array $bind = [
        "console" => Console::class,
        "config" => Config::class,
        "event" => Event::class,
        "route" => Route::class,
    ];


    public function __construct(ContainerInterface $container, string $rootPath)
    {
        $this->container = $container;
        $this->rootPath = $rootPath;
        $this->container->bind($this->bind);
        static::$instance = $this;
    }

    /**
     * 获取应用实例
     * @return App
     */
    public static function getInstance(): App
    {
        return static::$instance;
    }

    /**
     * 获取版本号
     * @return string
     */
    public function getVersion(): string
    {
        return self::VERSION;
    }

    /**
     * 初始化应用
     * @return void
     * @throws Exception
     */
    public function initialize()
    {
        $this->config->load($this->rootPath . DIRECTORY_SEPARATOR . 'config/');

        $appConfig = $this->config->get('app', []);
        $this->debug = $appConfig['debug'] ?? false;

        $phpIni = $appConfig['php-ini'] ?? [];
        foreach ($phpIni as $key => $value) {
            ini_set($key, $value);
        }

        $routeFiles = scandir($this->getRootPath() . 'route/');

        foreach ($routeFiles as $file) {
            if (in_array($file, ['.', '..'])) {
                continue;
            }

            if (is_file($this->getRootPath() . 'route/' . $file) && substr($file, -4) == '.php') {
                include_once $this->getRootPath() . 'route/' . $file;
            }
        }
        $this->route->parserRoute();

        //注册框架事件监听
        $events = $this->config->get('event', []);
        foreach ($events as $event => $listener) {
            $this->event->listen($event, $listener);
        }

        $this->event->trigger('initialize');
        $this->isInitialize = true;
    }

    /**
     * 获取初始化状态
     * @return bool
     */
    public function isInitialize(): bool
    {
        return $this->isInitialize;
    }


    /**
     * 获取应用根目录
     * @return string
     */
    public function getRootPath(): string
    {
        return $this->rootPath;
    }

    /**
     * 获取调试模式
     * @return bool
     */
    public function getDebug(): bool
    {
        return $this->debug;
    }


    public function __get($name)
    {
        return $this->container->make($name);
    }


}