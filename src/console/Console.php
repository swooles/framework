<?php

namespace Sws\Console;

use Sws\App;
use Sws\Console\Command\Help;
use Sws\Console\Command\Http;
use Sws\Console\Command\Version;

class Console
{
    /**
     * @var App
     */
    protected App $app;

    const INFO = 0;
    const SUCCESS = 1;
    const DEBUG = 2;
    const WARNING = 3;
    const ERROR = 4;

    /**
     * 命令列表
     * @var array|string[]
     */
    protected array $command = [
        'help' => Help::class,
        'version' => Version::class,
        'http' => Http::class,
    ];


    public function __construct(App $app)
    {
        $this->app = $app;
        if (!$this->app->isInitialize()) {
            $this->app->initialize();
        }
        $this->loadCommands();
    }

    /**
     * 加载命令
     * @return void
     */
    protected function loadCommands()
    {
        $command = $this->app->config->get('command', []);
        $this->command = array_merge($this->command, $command);
    }

    /**
     * 控制台运行
     * @return void
     */
    public function run()
    {
        $input = $_SERVER['argv'];
        array_shift($input);

        $name = array_shift($input) ?? 'help';

        if (in_array($name, ['-V', '--version'])) {
            $this->writeln($this->app->getVersion());
            return;
        }

        if (!isset($this->command[$name])) {
            throw new \LogicException(sprintf("Command '%s' not defined.", $name));
        }

        /**@var Command $command */
        $command = $this->app->container->make($this->command[$name], [$input, $this]);
        if (in_array($input[count($input) - 1] ?? '', ['-h', '--help'])) {
            $command->help();
            return;
        }

        $command->run();
    }


    /**
     * 获取命令列表
     * @return array|string[]
     */
    public function getCommands(): array
    {
        return $this->command;
    }


    /**
     * logo
     * @return string
     */
    public function logo(): string
    {
        return <<<EOL
                            .---.                    
      .--.--.              /. ./|     .--.--.        
     /  /    '          .-'-. ' |    /  /    '       
    |  :  /`./         /___/ \: |   |  :  /`./       
    |  :  ;_        .-'.. '   ' .   |  :  ;_         
     \  \    `.    /___/ \:     '    \  \    `.      
      `----.   \   .   \  ' .\        `----.   \     
     /  /`--'  /    \   \   ' \ |    /  /`--'  /     
    '--'.     /      \   \  |--"    '--'.     /      
      `--'---'        \   \ |         `--'---'      
                       '---"                                                                                                          
EOL;
    }

    public function writeln(string $content, int $level = self::INFO)
    {

        switch ($level) {
            case self::SUCCESS:
                $content = sprintf("\e[0;32m%s\e[0m", $content);
                break;
            case self::DEBUG:
                $content = sprintf("\e[0;36m%s\e[0m", $content);
                break;
            case self::WARNING:
                $content = sprintf("\e[0;33m%s\e[0m", $content);
                break;
            case self::ERROR:
                $content = sprintf("\e[41;37m%s\e[0m", $content);
                break;
        }
        $content .= PHP_EOL;
        $output = @fopen('php://output', 'w');

        if (false === @fwrite($output, $content)) {
            echo $content;
        } else {
            fflush($output);
        }
    }


}