<?php

namespace Sws\Console\Command;

use Sws\App;
use Sws\Console\Command;
use Sws\Console\Console;

class Version extends Command
{

    /**
     * @return void
     */
    public function configure()
    {
        $this->setName('version')
            ->setDesc('Displayed sws version.');
    }

    /**
     * @return void
     */
    public function execute()
    {
        $this->console->writeln('v' . App::VERSION);
    }
}