<?php

namespace Sws\Console\Command;

use Sws\App;
use Sws\Console\Command;
use Sws\Console\Console;

class Help extends Command
{

    public function configure()
    {
        $this->setName('help')
            ->setDesc('Displayed command help.');
    }

    public function execute()
    {
        $this->console->writeln($this->console->logo());
        $this->console->writeln('version:' . App::VERSION . PHP_EOL);
        $this->console->writeln('Usage:');
        $this->console->writeln('  command [arguments] [options]' . PHP_EOL);
        $this->console->writeln('Options:');
        $this->console->writeln(str_pad('  -h, --help', 22, ' ') . 'Display this help message.');
        $this->console->writeln(str_pad('  -V, --version', 22, ' ') . 'Display frame version.' . PHP_EOL);
        $this->console->writeln('Command list:');
        foreach ($this->console->getCommands() as $command => $class) {
            $desc = App::getInstance()->container->make($class, [[]], true)->getDesc();
            $this->console->writeln(str_pad('  ' . $command, 22, ' ') . $desc);
        }

    }

}