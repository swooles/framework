<?php

namespace Sws\Console\Command;

use Sws\App;
use Sws\Console\Command;

class Http extends Command
{

    public function configure()
    {
        $this->setName('http')
            ->addArgument('action', Command::REQUIRE, 'start|stop|restart|reload|status', 'start')
            ->addOption('daemonize', 'd', Command::OPTIONAL, 'Run the server in daemon mode.')
            ->addOption('config', 'c', Command::OPTIONAL, 'Server conf-path.')
            ->setDesc('Http server.');
    }

    public function execute()
    {
        $app = App::getInstance();

        $action = $this->getArgument('action');
        if (!in_array($action, ['start', 'stop', 'restart', 'reload', 'status',])) {
            throw new \LogicException(sprintf("Unsupported action '%s',input start|stop|restart|reload|status.", $action));
        }

        if ($this->hasOption('config')) {
            $config = require $this->hasOption('config');
        } else {
            $config = $app->config->get('server.http');
        }

        if ($this->hasOption('daemonize')) {
            $config['setting']['daemonize'] = true;
        }

        $http = $app->container->make(\Sws\Server\Http::class, [$app, $config]);
        $http->$action();
    }
}