<?php

namespace Sws\Console;

use Cassandra\Varint;
use RuntimeException;
use LogicException;

abstract class Command
{

    //必须传值
    const REQUIRE = 1;

    //可选传值
    const OPTIONAL = 2;

    //不需要传值
    const NONE = 3;

    //数组传值
    const ARRAY = 4;


    protected Console $console;

    /**
     * 指令名
     * @var string
     */
    protected string $name = '';

    /**
     * 参数
     * @var array
     */
    protected array $arguments = [];

    /**
     * 参数是否设置数组
     * @var bool
     */
    private bool $argumentArray = false;

    /**
     * 选项
     * @var array
     */
    protected array $options = [];

    /**
     * 命令描述
     * @var string
     */
    protected string $desc = '';

    /**
     * 命令行输入参数
     * @var array
     */
    protected array $input = [];

    public function __construct(array $input, Console $console)
    {
        $this->console = $console;
        $this->input = $input;
        $this->configure();

        if (!$this->name) {
            throw new LogicException(sprintf('The command class:"%s" undefined name.', get_class($this)));
        }
    }

    /**
     * 配置指令
     * @return mixed|void
     */
    abstract public function configure();

    /**
     * 执行指令
     * @return mixed|void
     */
    abstract public function execute();

    /**
     * 设置指令名
     * @param string $name
     * @return Command
     */
    public function setName(string $name): Command
    {
        $this->name = $name;
        return $this;
    }

    /**
     * 添加参数
     * @param string $name 参数名
     * @param int $type 参数类型
     * @param string $desc 描述
     * @param string|array $default 默认值
     * @return Command
     */
    public function addArgument(string $name, int $type = Command::OPTIONAL, string $desc = '', $default = null): Command
    {

        if ($type == Command::ARRAY) {
            if ($this->argumentArray) {
                throw new LogicException("Parameters cannot be set after array type parameters.");
            }
            $this->argumentArray = true;

            if (!is_null($default) && !is_array($default)) {
                $default = [$default];
            }
        }

        $argumentInfo = [
            'type' => $type,
            'desc' => $desc,
            'value' => null,
            'default' => $default,
        ];

        $this->arguments[$name] = $argumentInfo;
        return $this;
    }

    /**
     * 添加命令选项
     * @param string $name
     * @param string $shortcut
     * @param int $type
     * @param string $desc
     * @param null $default
     * @return $this
     */
    public function addOption(string $name, string $shortcut = '', int $type = Command::OPTIONAL, string $desc = '', $default = null): Command
    {
        if ($type == Command::ARRAY && !is_null($default) && !is_array($default)) {
            $default = [$default];
        }

        if (strlen($shortcut) > 1) {
            throw new LogicException("Shortcut only one letter is supported.");
        }

        $optionInfo = [
            'type' => $type,
            'shortcut' => $shortcut,
            'desc' => $desc,
            'value' => null,
            'default' => $default,
        ];

        $this->options[$name] = $optionInfo;
        return $this;
    }

    /**
     * 设置命令描述
     * @param string $desc
     * @return $this
     */
    public function setDesc(string $desc): Command
    {
        $this->desc = $desc;
        return $this;
    }

    /**
     * 获取命令描述
     * @return string
     */
    public function getDesc(): string
    {
        return $this->desc;
    }

    /**
     * 运行命令
     * @return void
     */
    public function run()
    {
        $this->parseCommand();
        $this->execute();
    }

    /**
     * 解析命令
     * @return void
     */
    protected function parseCommand()
    {
        $input = $this->input;
        $this->parseArgument($input);
        $this->parseOption($input);
    }

    /**
     * 解析参数
     * @param array $input
     * @return void
     */
    protected function parseArgument(array &$input)
    {
        foreach ($this->arguments as $name => &$argument) {
            if (isset($input[0]) && !$this->isOption($input[0])) {
                $value = array_shift($input);
            } else {
                $value = null;
            }
            $value = $value ?? $argument['default'];

            switch ($argument['type']) {
                case static::REQUIRE:
                    if (!$value) {
                        throw new LogicException(sprintf("Argument '%s' must input.", $name));
                    }
                    $argument['value'] = $value;
                    break;
                case static::OPTIONAL:
                    if ($value) {
                        $argument['value'] = $value;
                    }
                    break;
                case static::ARRAY:
                    if ($value) {
                        $argument['value'] = [$value];
                    }

                    while (count($input) > 0) {
                        if (!$this->isOption($input[0])) {
                            $argument['value'][] = array_shift($input);
                        } else {
                            break;
                        }
                    }
                    break;
            }
        }
    }

    /**
     * 解析选项
     * @param array $input
     * @return void
     */
    protected function parseOption(array &$input)
    {
        $nowOption = null;
        $inputOption = '';

        while (count($input) > 0) {
            $value = array_shift($input);

            if ($this->isOption($value)) {
                //短选项
                if (substr($value, 1, 1) != '-') {
                    $name = substr($value, 1, 1);
                    $name = $this->getOptionNameByShortcut($name);

                    if (!$name) {
                        throw new RuntimeException(sprintf("Command option '%s' not defined", $value));
                    }

                    if (substr($value, 2)) {
                        array_unshift($input, substr($value, 2));
                    }
                } else {
                    $name = substr($value, 2);

                    if (!isset($this->options[$name])) {
                        throw new RuntimeException(sprintf("Command option '%s' not defined", $value));
                    }
                }

                $nowOption = $name;
                $inputOption = $value;

                if ($this->options[$name]['value'] === null) {
                    if ($this->options[$name]['type'] == static::ARRAY) {
                        $this->options[$name]['value'] = [];
                    } else {
                        $this->options[$name]['value'] = '';
                    }
                }

            } else {

                if (!$nowOption) {
                    throw new RuntimeException(sprintf("Invalid option value '%s'", $value));
                }

                if ($this->options[$nowOption]['type'] == static::NONE) {
                    throw new RuntimeException(sprintf("Options '%s' do not require a value.", $inputOption));
                }

                if ($this->options[$nowOption]['type'] == static::ARRAY) {
                    if (empty($this->options[$nowOption]['value'])) {
                        $this->options[$nowOption]['value'] = [$value];
                    } else {
                        $this->options[$nowOption]['value'][] = $value;
                    }
                } else {
                    $this->options[$nowOption]['value'] = $value;
                }
            }
        }

        foreach ($this->options as $name => &$option) {
            if ($option['value'] === null && $option['default'] !== null) {
                $option['value'] = $option['default'];
            }

            if ($option['type'] == static::REQUIRE && $option['value'] === null) {
                throw new LogicException(sprintf("Option '%s' must input.", $name));
            }
        }

    }

    /**
     * 是否设置选项
     * @param string $name
     * @return bool
     */
    public function hasOption(string $name): bool
    {
        return isset($this->options[$name]) && $this->options[$name]['value'] !== null;
    }

    /**
     * 获取命令选项
     * @param string $name
     * @param string $key
     * @return mixed|null
     */
    public function getOption(string $name, string $key = 'value')
    {
        if (!isset($this->options[$name])) {
            return null;
        }
        return $key ? $this->options[$name][$key] : $this->options[$name];
    }

    /**
     * 是否设置参数
     * @param string $name
     * @return bool
     */
    public function hasArgument(string $name): bool
    {
        return isset($this->arguments[$name]) && $this->arguments[$name]['value'] !== null;
    }

    /**
     * 获取命令参数
     * @param string $name
     * @param string $key
     * @return mixed|null
     */
    public function getArgument(string $name, string $key = 'value')
    {
        if (!isset($this->arguments[$name])) {
            return null;
        }
        return $key ? $this->arguments[$name][$key] : $this->arguments[$name];
    }

    /**
     * 通过短选项获取选项名
     * @param string $shortcut
     * @return false|int|string
     */
    protected function getOptionNameByShortcut(string $shortcut)
    {
        foreach ($this->options as $name => $option) {
            if ($option['shortcut'] == $shortcut) {
                return $name;
            }
        }
        return false;
    }

    /**
     * 是否是选项
     * @param string $value
     * @return bool
     */
    protected function isOption(string $value): bool
    {
        return preg_match('/^--?[A-Za-z]+$/', $value) === 1;
    }

    /**
     * 显示命令帮助
     * @return void
     */
    public function help()
    {
        $description = 'Description:' . PHP_EOL . "  {$this->desc}" . PHP_EOL . PHP_EOL;
        $usage = 'Usage:' . PHP_EOL;
        $usage .= '  ' . $this->name;
        $arguments = PHP_EOL . 'Arguments:' . PHP_EOL;
        $options = PHP_EOL . 'Options:' . PHP_EOL;

        foreach ($this->arguments as $key => $value) {
            $name = '';
            $argument = '';
            switch ($value['type']) {
                case static::REQUIRE:
                    $name .= " <$key>";
                    break;
                case static::OPTIONAL:
                    $name .= " [<$key>]";
                    break;
                case static::ARRAY:
                    $name .= " [<$key>]...";
                    break;
            }
            $argument .= str_pad("  $key", 22, ' ') . $value['desc'];
            if ($value['default'] !== null) {
                $v = '';
                $this->valueType($value['default'], $v);
                $argument .= " [default:$v]";
            }

            $usage .= $name;
            $arguments .= $argument . PHP_EOL;
        }

        $usage .= ' [options]' . PHP_EOL;

        foreach ($this->options as $key => $value) {
            $option = '';
            if ($value['shortcut']) {
                $option .= "  -{$value['shortcut']}, ";
            } else {
                $option .= '    ';
            }

            $option .= '--' . $key;
            $option = str_pad($option, 22, ' ') . $value['desc'];

            if ($value['default'] !== null) {
                $v = '';
                $this->valueType($value['default'], $v);
                $option .= " [default:$v]";
            }
            $options .= $option . PHP_EOL;
        }

        $this->console->writeln($description . $usage . $arguments . $options);
    }

    private function valueType($value, string &$string)
    {
        switch (gettype($value)) {
            case 'boolean':
                if ($value) {
                    $string .= 'true';
                } else {
                    $string .= 'false';
                }
                break;
            case 'string':
                $string .= '"' . $value . '"';
                break;
            case 'array':
                $value = array_values($value);
                $string .= '[';
                foreach ($value as $key => $item) {
                    if ($key > 0) {
                        $string .= ',';
                    }
                    $this->valueType($item, $string);
                }
                $string .= ']';
                break;
            default:
                $string .= $value;
        }
    }

}