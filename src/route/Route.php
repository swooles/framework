<?php

namespace Sws\Route;

use Closure;

class Route
{

    /**
     * 路由解析树
     * @var array
     */
    private array $routeTree = ['static' => [], 'dynamic' => []];

    /**
     * 路由对象列表
     * @var array
     */
    private array $routeRuleList = [];

    /**
     * 当前路由分组
     * @var RouteGroup
     */
    private RouteGroup $routeGroup;

    /**
     * 初始化一个默认的路由分组
     */
    public function __construct()
    {
        $this->routeGroup = new RouteGroup();
    }

    /**
     * 添加分组
     * @param string $groupName
     * @param callable $callback
     * @return RouteGroup
     */
    public function group(string $groupName, callable $callback): RouteGroup
    {
        $previousRouteGroup = $this->routeGroup;
        $routeGroup = new RouteGroup($previousRouteGroup, $groupName);
        $this->routeGroup = $routeGroup;
        $callback();
        $this->routeGroup = $previousRouteGroup;
        return $routeGroup;
    }

    /**
     * GET路由
     * @param string $route
     * @param        $handler
     * @return RouteRule
     */
    public function get(string $route, $handler): RouteRule
    {
        return $this->addRoute('GET', $route, $handler);
    }

    /**
     * POST路由
     * @param string $route
     * @param        $handler
     * @return RouteRule
     */
    public function post(string $route, $handler): RouteRule
    {
        return $this->addRoute('POST', $route, $handler);
    }

    /**
     * DELETE路由
     * @param string $route
     * @param        $handler
     * @return RouteRule
     */
    public function delete(string $route, $handler): RouteRule
    {
        return $this->addRoute('DELETE', $route, $handler);
    }

    /**
     * HEAD路由
     * @param string $route
     * @param        $handler
     * @return RouteRule
     */
    public function head(string $route, $handler): RouteRule
    {
        return $this->addRoute('HEAD', $route, $handler);
    }

    /**
     * PATCH路由
     * @param string $route
     * @param        $handler
     * @return RouteRule
     */
    public function patch(string $route, $handler): RouteRule
    {
        return $this->addRoute('PATCH', $route, $handler);
    }

    /**
     * PUT路由
     * @param string $route
     * @param        $handler
     * @return RouteRule
     */
    public function put(string $route, $handler): RouteRule
    {
        return $this->addRoute('PUT', $route, $handler);
    }

    /**
     * OPTION路由
     * @param string $route
     * @param        $handler
     * @return RouteRule
     */
    public function option(string $route, $handler): RouteRule
    {
        return $this->addRoute('OPTION', $route, $handler);
    }

    /**
     * ANY路由
     * @param string $route
     * @param        $handler
     * @return RouteRule
     */
    public function any(string $route, $handler): RouteRule
    {
        return $this->addRoute('*', $route, $handler);
    }

    /**
     * 添加路由
     * @param        $method
     * @param string $route
     * @param        $handler
     * @return RouteRule
     */
    public function addRoute($method, string $route, $handler): RouteRule
    {
        $routeRule = $this->routeGroup->addRoute($method, $route, $handler);
        $this->routeRuleList[] = $routeRule;
        return $routeRule;
    }

    /**
     * 解析路由
     * @return void
     * @throws \Exception
     */
    public function parserRoute()
    {
        /**@var RouteRule $routeRule * */
        foreach ($this->routeRuleList as $routeRule) {
            $tree = $routeRule->getRouteTree();
            foreach ($tree as $type => $value1) {
                foreach ($value1 as $method => $value2) {
                    foreach ($value2 as $url => $value3) {
                        if (isset($this->routeTree[$type][$method][$url])) {
                            continue;
                        } else {
                            $this->routeTree[$type][$method][$url] = $value3;
                        }
                    }
                }
            }
        }
        unset($this->routeRuleList);
    }

    /**
     * 获取路由树
     * @return array|array[]
     */
    public function getRouteTree(): array
    {
        return $this->routeTree;
    }

    /**
     * 获取路由列表
     * @return array
     */
    public function getRouteList(): array
    {
        $list = [];
        foreach ($this->routeTree as $type => $tree) {
            foreach ($tree as $method => $routeArray) {
                foreach ($routeArray as $r => $item) {
                    $route['type'] = $type;
                    $route['method'] = $method;
                    $route['route'] = $r;
                    switch (1) {
                        case is_array($item['handle']):
                            if (is_string($item['handle'][0])) {
                                $route['handle'] = $item['handle'][0] . '::' . $item['handle'][1];
                            }
                            if (is_object($item['handle'][0])) {
                                $route['handle'] = get_class($item['handle'][0]) . '::' . $item['handle'][1];
                            }
                            break;
                        case $item['handle'] instanceof Closure:
                            $route['handle'] = 'function()';
                            break;
                        default:
                            $route['handle'] = (string)$list['handle'];
                    }
                    $list[] = $route;
                }
            }
        }
        return $list;
    }


    /**
     * 匹配路由
     * @param string $method
     * @param string $uri
     * @return array|null
     */
    public function match(string $method, string $uri): ?array
    {
        if (isset($this->routeTree['static'][$method][$uri])) {
            return [false, $this->routeTree['static'][$method][$uri]];
        }

        $tree = $this->routeTree['dynamic'][$method] ?? [];
        foreach ($tree as $key => $value) {
            if (preg_match('{^' . $key . '$}', $uri, $matches)) {
                $value['matches'] = $matches;
                return [true, $value];
            }
        }
        return null;
    }

}




